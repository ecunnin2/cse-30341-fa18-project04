#!/bin/bash

# Functions

time-library() {
    library=$1
    echo -n "Timing $library BIG... "
    { time env LD_PRELOAD=./lib/$library ./bin/test_big; } |& awk '/real\s+/ { print $2 }'
    echo -n "Timing $library MED... "
    { time env LD_PRELOAD=./lib/$library ./bin/test_medium; } |& awk '/real\s+/ { print $2 }'
    echo -n "Timing $library SM... "
    { time env LD_PRELOAD=./lib/$library ./bin/test_small; } |& awk '/real\s+/ { print $2 }'
    #echo -n "Output of $library trial... "
    #{ env LD_PRELOAD=./lib/$library ./bin/test_04; }
}

# Main execution

time-library libmalloc-ff.so
time-library libmalloc-bf.so
time-library libmalloc-wf.so

# vim: sts=4 sw=4 ts=8 ft=sh
