#!/bin/bash

# Functions

test-library() {
library=$1
echo -n "Testing $library ... "
{ env LD_PRELOAD=./lib/$library ./bin/test_output; }
}

# Main execution

test-library libmalloc-ff.so
test-library libmalloc-bf.so
test-library libmalloc-wf.so

# vim: sts=4 sw=4 ts=8 ft=sh
