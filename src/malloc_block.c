/* malloc_block.c: Block Structure */

#include "malloc/block.h"
#include "malloc/counters.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

/* Functions */

/**
 * Allocate a new block on the heap using sbrk:
 *
 *  1. Determined aligned amount of memory to allocate.
 *  2. Allocate memory on the heap.
 *  3. Set allocage block properties.
 *
 * @param   size    Number of bytes to allocate.
 * @return  Pointer to data portion of newly allocate block.
 **/
Block *	block_allocate(size_t size) {
    intptr_t capacity = ALIGN(size + sizeof(Block));
    Block *  block    = sbrk(capacity);
    if (block == SBRK_FAILURE) {
        return NULL;
    }
    
    block->size = size;
    block->prev = block;
    block->next = block;

    Counters[HEAP_SIZE] += capacity;
    Counters[BLOCKS]++;
    Counters[GROWS]++;
    return block;
}

/**
 * Attempt to release memory used by block to heap:
 *
 *  1. If the block is at the end of the heap.
 *  2. The block size meets the trim threshold.
 *
 * @param   block   Pointer to block to release.
 * @return  Whether or not the release completed successfully.
 **/
bool	block_release(Block *block) {
    size_t allocated = ALIGN(sizeof(Block) + block->size);
    
    if (((intptr_t)sbrk(0) != (intptr_t)(block->data+block->size)) || allocated < TRIM_THRESHOLD) {
        return false;
    }
    
    if (sbrk(-allocated) == SBRK_FAILURE){
        return false;
    }
    
    Counters[BLOCKS]--;
    Counters[SHRINKS]++;
    Counters[HEAP_SIZE] -= allocated;
    return true;
}

/**
 * Detach specified block from its neighbors.
 *
 * @param   block   Pointer to block to detach.
 * @return  Pointer to detached block.
 **/
Block * block_detach(Block *block) {
    /* Update prev and next blocks to reference each other */
    if (block) {
        Block *prev = block->prev;
        Block *next = block->next;
        prev->next  = block->next;
        next->prev  = block->prev;
        block->next = block;
        block->prev = block;
    }
    
    return block;

}

/**
 * Attempt to merge source block into destination.
 *
 *  1. Compute end of destination and start of source.
 *
 *  2. If they both match, then merge source into destionation by giving the
 *  destination all of the memory allocated to source.
 *
 * @param   dst     Destination block we are merging into.
 * @param   src     Source block we are merging from.
 * @return  Whether or not the merge completed successfully.
 **/
bool	block_merge(Block *dst, Block *src) {
    intptr_t ptr1 = (intptr_t)(dst->data + ALIGN(dst->size));
    intptr_t ptr2 = (intptr_t)src;
    
    if (ptr1 != ptr2) {
        return false;
    }
    dst->size = ALIGN(dst->size) + ALIGN(src->size + sizeof(Block));
    Counters[MERGES]++;
    Counters[BLOCKS]--;
    return true;
}

/**
 * Attempt to split block with the specified size:
 *
 *  1. Check if aligned block size is sufficient for requested size and header
 *  Block.
 *  2. Split specified block into two blocks.
 *
 * @param   block   Pointer to block to split into two separate blocks.
 * @param   size    Desired size of the first block after split.
 * @return  Pointer to original block if the split was successfully or NULL if a
 * split was not possible.
 **/
Block * block_split(Block *block, size_t size) {
    if (!block) {
        return NULL;
        
    }
    
    if (ALIGN(block->size) < ALIGN(size + sizeof(Block))) {
        return NULL;
    }
    
    Block *middle = (Block *)(block->data + ALIGN(size));
    middle->size = (block->size - sizeof(Block) - ALIGN(size));
    block->size = size;
    Block *next = block->next;
    
    block->next = middle;
    middle->prev = block;
    middle->next = next;
    next->prev = middle;
    
    Counters[SPLITS]++;
    Counters[BLOCKS]++;
    return block;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
