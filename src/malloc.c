/* malloc.c: POSIX API Implementation */

// env LD_PRELOAD=./lib/libmalloc-ff.so ./bin/test_00

#include "malloc/counters.h"
#include "malloc/freelist.h"

#include <assert.h>
#include <string.h>

/* Functions */
/**
 * Allocate specified amount memory.
 * @param   size    Amount of bytes to allocate.
 * @return  Pointer to the requested amount of memory.
 **/
void *malloc(size_t size) {
    /* Initialize counters */
    init_counters();

    /* Handle empty size */
    if (!size) {
        return NULL;
    }

    // Search free list for any available block with matching size.
    Block *block = free_list_search(size);
    
    if (block && block->size > size) {
        block = block_split(block, size);
    }
    
    if (!block) {
        block = block_allocate(size);
    } else {
        block = block_detach(block);
    }

    /* Could not find free block or allocate a block, so just return NULL */
    if (!block) {
        return NULL;
    }
    
    //fprintf(stderr, "Requested Size %lu\n", size);
    //fprintf(stderr, "Block Size %lu\n", block->size);
    
    assert(block->size == size);
    assert(block->next == block);
    assert(block->prev == block);

    /* Update counters */
    Counters[MALLOCS]++;
    Counters[REQUESTED] += size;
    
    /* Return data address associated with block */
    return block->data;
}

/**
 * Release previously allocated memory.
 * @param   ptr     Pointer to previously allocated memory.
 **/
void free(void *ptr) {
    if (!ptr) {
        return;
    }

    Block *block = BLOCK_FROM_POINTER(ptr);
    /* Update counters */
    Counters[FREES]++;
    
    
    // Try to release block, otherwise insert it into the free list.
    if (!block_release(block)) {
        free_list_insert(block);
    }
    
}

/**
 * Allocate memory with specified number of elements and with each element set
 * to 0.
 * @param   nmemb   Number of elements.
 * @param   size    Size of each element.
 * @return  Pointer to requested amount of memory.
 **/
void *calloc(size_t nmemb, size_t size) {
    size_t total = (nmemb * size);
    void *p = malloc(total);
    
    if (p) {
        memset(p,0,total);
        Counters[CALLOCS]++;
        return p;
    }
    return NULL;
}

/**
 * Reallocate memory with specified size.
 * @param   ptr     Pointer to previously allocated memory.
 * @param   size    Amount of bytes to allocate.
 * @return  Pointer to requested amount of memory.
 **/
void *realloc(void *ptr, size_t size) {
    void *reallocedBlock = malloc(size);
    
    if (reallocedBlock && ptr) {
        Block *block = BLOCK_FROM_POINTER(ptr);
        
        memcpy(reallocedBlock, ptr, block->size);
    }
    free(ptr);
    Counters[REALLOCS]++;
    return reallocedBlock;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
